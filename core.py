import subprocess
import re
import logging

logger = logging.getLogger("HA-Agent")

class Iptables:
    def __get_NAT_table_list(self):
        try:
            res = subprocess.check_output(['iptables', '-w', '-t', 'nat', '-S'], encoding='utf-8')
        except subprocess.CalledProcessError:
            logger.error("Cannot fetch iptables rules. Are you root?")
            exit(1)
        lst = res.splitlines()
        return lst
    def __init__(self):
        self.nat_list = self.__get_NAT_table_list()
    def update_NAT_table_list(self):
        self.nat_list = self.__get_NAT_table_list()
    
    """ Fetch probability == 1.0 rules in iptables' NAT table
        Args:
            lst: iptables NAT table rule raw output (-S option)
        Returns
            list of valid kubernetes HA rules
    """
    def fetch_target_rules_list(self):
        target = []
        for rule in self.nat_list:
            if "--probability 1.00000000000" in rule:
                target.append(rule)
                logger.debug("Target Rule: %s", rule)
            
        return target
    
    """ Get (KUBE-SVC chain, Namespace, App name, Service name, KUBE-SEP chain) tuple from iptables rule
        Args:
            rule: kubernetes iptables rule which contains "--probability 1.00000000000"
        Returns:
            (KUBE-SVC chain, Namespace, Service_name, App_name, KUBE-SEP chain)
    """
    def parse_data_from_rule(self, rule):
        regex = re.compile("(KUBE-SVC-\w*).*--comment \"(.*)\/(.*):(.*)\".* -j (.*)")
        res = regex.search(rule)
        # KUBE-SVC Chain
        kube_svc = res.group(1)
        # namespace
        ns = res.group(2)
        # app == servic_name 
        app = res.group(3)
        # service port name
        svc = res.group(4)
        # KUBE-SEP Chain
        kube_sep = res.group(5)
        logger.debug("KUBE-SVC Chain: %s, Namespace: %s, App Name: %s, Service Port: %s, KUBE-SEP: %s", kube_svc, ns, app, svc, kube_sep)

        return (kube_svc, ns, svc, app, kube_sep)
    
    """ Get Cluster IP from target service
        Args:
            lst: iptables NAT table rule raw output(-S option)
            kube_svc: target KUBE-SVC Chain
        Return:
            clusterIP: target cluster IP Address
    """
    def get_clusterIP(self, kube_svc):
        for rule in self.nat_list:
            if "KUBE-SERVICES -d" in rule and kube_svc in rule:
                regex = re.compile("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b") #IPv4
                res = regex.search(rule)
                clusterIP = res.group(0)
                logger.debug("Cluster IP: %s", clusterIP)
                return clusterIP
            else:
                continue
        return None

    def get_podIP(self, kube_sep):
        for rule in self.nat_list:
            if "--to-destination" in rule and kube_sep in rule:
                regex = re.compile("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b") #IPv4
                res = regex.search(rule)
                podIP = res.group(0)
                logger.debug("Pod IP: %s", podIP)
                return podIP
            else:
                continue
        return None

    """ Delete target rule from iptables.
        Args:
            rule: target rule. it MUST NOT contains chain Action,
        Return:
            None
    """
    def delete_rule(self, rule):
        for i in range(0, 1000):
            try:
                command = 'iptables -t nat -D' + rule
                iptables = subprocess.run(command, shell=True, check=True)
            except subprocess.CalledProcessError:
                logger.warning("iptables locked. retry...")
                time.sleep(random.randint(10,20)/10000)
                continue
            break

class Docker:
    def __get_docker_ps_list(self):
        try:
            res = subprocess.check_output(['docker', 'ps'], encoding='utf-8')
        except OSError:
            logger.error("err:docker client is doesn't exist.")
            exit(1)
        except subprocess.CalledProcessError:
            logger.error("err:docker cannot fetch containers. Are you root?")
            exit(1)

        return res.splitlines()[1:]
    def __init__ (self):
        self.docker_ps_list = self.__get_docker_ps_list()
    def update_docker_ps_list(self):
        self.docker_ps_list = self.__get_docker_ps_list()

    """ Fetch target k8s pod containers
        Args:
            app: k8s app name metadata
            namespace: k8s namespace of target pods
        Return:
            List of target containers' ID
    """
    def fetch_pod_containers(self, app, namespace):
        cids = []
        match_str = "k8s_.*_" + app + "-.*_default_"
        logger.debug("Match regex string: %s", match_str)
        regex = re.compile(match_str)
        for line in self.docker_ps_list:
            if regex.search(line) and "/pause" not in line: #Ignore pause container
                cids.append(line.split()[0])
                logger.debug("Containder ID Found: %s", line.split()[0])
        if len(cids) is 0:
            return None
        else:
            return cids
    
    """ Get container PID from container ID
        Args:
            cid: container ID
        Return
            PID: Process ID of target container
    """
    def get_container_pid(self, cid):
        #TODO: bottleneck point
        docker = subprocess.check_output(['docker', 'inspect', cid, '-f', "{{.State.Pid}}"], encoding='utf-8')
        return docker.strip()

class HA_Iface:
    def __init__(self):
        self.__docker = Docker()
        self.__iptables = Iptables()
    def fetch_target_rules_list(self):
        return self.__iptables.fetch_target_rules_list()
    def parse_data_from_rule(self, rule):
        return self.__iptables.parse_data_from_rule(rule)
    def delete_rule(self, rule):
        return self.__iptables.delete_rule(rule)
    
    def get_container_id(self, app, namespace, kube_sep):
        #TODO: Validate not None
        podIP = self.__iptables.get_podIP(kube_sep)
        cids = self.__docker.fetch_pod_containers(app, namespace)
        if cids is None:    
            print("%s %s" % (app, namespace))
        pids = []
        for cid in cids:
            pid = self.__docker.get_container_pid(cid)
            pids.append(pid)
        target_pid = None
        target_cid = None
        for pid in pids:
            nsenter = subprocess.check_output(['nsenter', '-t', pid, '-n', 'ip', 'addr', 'show', 'dev', 'eth0'], encoding='utf-8')
            lines = nsenter.splitlines()
            for line in lines:
                if podIP in line:
                    target_pid = pid
                    break
        if target_pid is not None:
            idx = pids.index(target_pid)
            target_cid = cids[idx]

        if target_cid is not None:
            return target_cid
        else:
            return None

    def get_container_pid(self, cid):
        return self.__docker.get_container_pid(cid)
    def get_podIP(self, kube_sep):
        return self.__iptables.get_podIP(kube_sep)
    def get_clusterIP(self, kube_svc):
        return self.__iptables.get_clusterIP(kube_svc)

    def update_docker_ps_list(self):
        return self.__docker.update_docker_ps_list()
    def update_NAT_table_list(self):
        return self.__iptables.update_NAT_table_list()
