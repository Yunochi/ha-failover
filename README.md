HA-Failover Agent
=================
1. Overview
----------------
Goal of this project is surpressing network downtime under 20ms when container down.
To acheive this goal, we're used getpgid() syscall, for detect target process is alive.  
Why using PID? UNIX socket is unstable and consumes a few cpu. It limits scalablility.
Inotify is too slow and operation is not guaranteed depending on kernel version.  
A project also require common solution which applicapable to every container.  

2. Limitations
----------------
- iptables is locked when fetch and delete rules. if HA containers died continuously, network downtime is increase
- iptables using sequential search. If NAT table contains very large amount of rules, downtime can excess 20ms
- Target pod's service name and app name is MUST be same
- Modififed kube-proxy binary is required for fixing target possibility to 1.0

3. Installation
-----------------
#### Install packages
    yum install python3-devel
#### Install python3 dependency
    pip3 install -r requirements.txt
#### Copy files to /opt/ha-agent/
    cp core.py ha-agent.py model.py pidprobe /opt/ha-agent
#### Copy service manifest to systemd directory
    cp ha-agent.service /etc/systemd/system
#### Enable HA-Agent service on systemd
    systemctl enable ha-agent
#### Start HA-Agent service
    systemctl start ha-agent