from model import Model
from core import HA_Iface

import time
import subprocess
import os, sys, signal
from multiprocessing import Process
import psutil
import logging
import argparse
import shutil

logger = logging.getLogger("HA-Agent")
PROBE_BINARY = "./pidprobe"
PID_FILE = "/var/run/ha-agent.pid"
NODE_EXPORTER_TRIGGER_DIR = "/tmp/k8s-shared/"
NODE_EXPORTER_TRIGGER_FILE = "failover"

class HAAgent:
    def __init__(self):
        self.__stop = False
        self.__iface = HA_Iface()
        self.instances = []
        #Add signal handler
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)

    def stop(self, signum, frame):
        logger.info("Gracefully shutdown HA-Agent")
        self.__stop = True
        for ins in self.instances:
            logger.info("Terminate running child procs. App: %s PID: %s", ins.app_name, ins.pidprobe_process.pid)
            proc = psutil.Process(ins.pidprobe_process.pid)
            for child in proc.children():
                child.terminate()
            proc.terminate()
        
        os.remove(PID_FILE)
        exit(0)
        
    """ wrapper for pidprobe
        Args:
            ins: Model() instance
        Returns: None
    """
    def pidprobe_run(self, ins):
        # Signal handler
        def pidprobe_stop(signum, frame):
            exit(0)
        # Override signal handler
        signal.signal(signal.SIGINT, pidprobe_stop)
        signal.signal(signal.SIGTERM, pidprobe_stop)
        
        logger.debug("%s: child python proc PID: %s", ins.app_name, ins.pidprobe_process.pid)
        try:
            res = subprocess.Popen([PROBE_BINARY, "--pid", ins.container_pid], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            logger.debug("%s: Pidprobe proc PID: %s", ins.app_name, res.pid)
            res.wait()
            returncode = res.returncode
        except subprocess.CalledProcessError:
            logger.error("%s: Caught unknown error.", ins.app_name)
            return False
        except Exception as e:
            logger.error("Subprocess got exception: %s", str(e))
            return False
        if returncode is 0:
            # Failover Action
            logger.info("%s:Failover start.", ins.app_name)
            rule = ins.target_rule[2:] #Trim '-A'
            self.__iface.delete_rule(rule)
            logger.info("%s:Failover complete.", ins.app_name)
            
            #Set Node Exporter trigger
            f = open(NODE_EXPORTER_TRIGGER_DIR + NODE_EXPORTER_TRIGGER_FILE, "w")
            f.write("1")
            f.close()
            return True
        else:
            logger.error("%s: Pidprobe is unexpectedly terminated.", ins.app_name)
            return False

    def HA_instance_builder(self, rule):
        ret = self.__iface.parse_data_from_rule(rule)
        kube_svc = ret[0]
        ns = ret[1]
        svc = ret[2]
        app = ret[3]
        kube_sep = ret[4]

        ins = Model()
        ins.target_rule = rule
        ins.container_id = self.__iface.get_container_id(app, ns, kube_sep)
        ins.container_pid = self.__iface.get_container_pid(ins.container_id)
        ins.clusterIP = self.__iface.get_clusterIP(kube_svc)
        ins.podIP = self.__iface.get_podIP(kube_sep)
        ins.app_name = app
        p = Process(target=self.pidprobe_run, args=(ins,))
        ins.pidprobe_process = p
        logger.info("%s: Start watching PID: %s", ins.app_name, ins.container_pid)
        p.start()
       
        return ins
    """ Main function. running HA Container monitoring/failover
        Args:
            refresh_interval: docker ps and iptables NAT Table's refresh
                              interval, sync monitoring target.
        Returns: None
    """
    def run(self, refresh_interval):
        #Initialize
        logger.info("Start HA-Agent")

        rules = self.__iface.fetch_target_rules_list()
        for rule in rules:
            logger.debug("Add rule: %s", rule)
            ins = self.HA_instance_builder(rule)
            self.instances.append(ins)

        while(not self.__stop):
            time.sleep(int(refresh_interval))
            # Check process is exited
            ins_exited = []
            for ins in self.instances:
                if ins.pidprobe_process.exitcode is not None:
                    ins.pidprobe_process.join() #Terminate exited process
                    logger.debug("Remove exited process: %s", ins.pidprobe_process.pid)
                    ins_exited.append(ins)
            for ins in ins_exited:
                self.instances.remove(ins)

            # Update iptables/docker ps
            self.__iface.update_docker_ps_list()
            self.__iface.update_NAT_table_list()
            # Check changes
            rules = self.__iface.fetch_target_rules_list()

            rules_old = []
            ins_old = []
            for ins in self.instances:
                for rule in rules:
                    if ins.target_rule == rule:
                        rules_old.append(rule)
                        ins_old.append(ins)
                        break
            
            ins_orphan = list(set(self.instances)-set(ins_old))
            rules_new = list(set(rules)-set(rules_old))

            # Remove orphan Model's parent/child proc
            for ins in ins_orphan:
                logger.warning("Terminate old rule's process and its child. App: %s PID:%s", ins.app_name, ins.pidprobe_process.pid)
                proc = psutil.Process(ins.pidprobe_process.pid)
                for child in proc.children():
                    child.terminate()
                proc.terminate()
                self.instances.remove(ins)
            # Watching new rules
            for rule in rules_new:
                ins = self.HA_instance_builder(rule)
                self.instances.append(ins)

def daemon_init(workdir):
    pid = os.fork()
    if pid > 0:
        exit()
    else:
        os.chdir(workdir)
        os.setsid()
        os.umask(0)

        pid = os.fork()
        if pid > 0:
            exit(0)
        else:
            sys.stdout.flush()
            sys.stderr.flush()
            si = open(os.devnull, 'r')
            so = open(os.devnull, 'a+')
            se = open(os.devnull, 'a+')

            os.dup2(si.fileno(), sys.stdin.fileno())
            os.dup2(so.fileno(), sys.stdout.fileno())
            os.dup2(se.fileno(), sys.stderr.fileno())

            return

if __name__ == "__main__":
    # Node Exporter Trigger
    if not os.path.isdir(NODE_EXPORTER_TRIGGER_DIR):
        os.mkdir(NODE_EXPORTER_TRIGGER_DIR)
    f = open(NODE_EXPORTER_TRIGGER_DIR + NODE_EXPORTER_TRIGGER_FILE, "w")
    shutil.chown(NODE_EXPORTER_TRIGGER_DIR + NODE_EXPORTER_TRIGGER_FILE, user='nobody', group='nogroup')
    f.write("0")
    f.close()
    
    # Argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--log-file", help="(str) Log filename. default=ha-agent.log", default="ha-agent.log", dest='log_file')
    parser.add_argument("--log-dir", help="(str) Log directory. default=/var/log", default="/var/log/", dest='log_dir')
    parser.add_argument("--log-level", help="(str) Set log level. default=INFO, valid value=(WARN, INFO, DEBUG)", default="INFO", dest='log_level')
    parser.add_argument("--refresh-interval", help="(int) Specify monitor sync interval(sec). default=30", default=30, dest='refresh_interval')
    parser.add_argument("--mode", help="(str) Running mode. valid value=(Foreground, Daemon). default=Foreground", default="Foreground", dest='mode')
    parser.add_argument("--workdir", help="(str) Set working directory for ha-agent daemon., default=/opt/ha-agent/", default="/opt/ha-agent/", dest='workdir')
    args = parser.parse_args()
    
    # Init logger
    # Print log to console
    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)
    
    # Set formatter
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] (%(funcName)s): %(message)s')
    stream_handler.setFormatter(formatter)
    # Set file handler
    try:
        file_handler = logging.FileHandler(args.log_dir + args.log_file)
    except:
        logger.error("Invalid log location: %s. set to default location..", args.log_dir + args.log_file)
        file_handler = logging.FileHandler("/var/log/ha-agent.log")
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # Set Log level
    if args.log_level == "WARN":
        logger.setLevel(logging.WARNING)
        logger.info("Log level: WARNING")
    elif args.log_level == "INFO":
        logger.setLevel(logging.INFO)
        logger.info("Log level: INFO")
    elif args.log_level == "DEBUG":
        logger.setLevel(logging.DEBUG)
        logger.info("Log level: DEBUG")
    else:
        logger.setLevel(logging.INFO)
        logger.error("Invalid log level: %s. set default log level: INFO", args.log_level)
    
    logger.info("Log file: %s", args.log_dir + args.log_file)
    logger.info("Running mode: %s", args.mode)
    # Prevent duplicate execution
    if os.path.isfile(PID_FILE):
        logger.warning("PID File is already exist!")
        f = open(PID_FILE, "r")
        pid = f.readlines()[0]
        f.close()
        if psutil.pid_exists(int(pid)):
            logger.critical("HA-Agent is running!")
            exit(1)
        else:
            logger.info("HA-Agent is unexpectdly exited last time.")

    # Daemonize
    if args.mode == "Daemon":
        daemon_init(args.workdir)
    
    # Set PID File 
    f = open(PID_FILE, "w")
    pid = str(os.getpid())
    f.write(pid)
    f.close()
    
    # Run HA-Agent
    agent = HAAgent()
    agent.run(args.refresh_interval)


